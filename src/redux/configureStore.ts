import { createStore, Store, compose } from "redux";
import rootReducer from "./modules";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store: Store;

const createReduxStore: () => Store = () => {
  store = createStore(rootReducer, composeEnhancers());
  return store;
};

const getReduxStore: () => Store = () => {
  return store || createReduxStore();
};

export default getReduxStore();
