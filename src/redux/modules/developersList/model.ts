import devsList from "../../../mock_data/devs_list";

export const defaultDevsListState: DevelopersListState = {
  list: devsList,
  activeDeveloperId: 0,
};

export interface DevelopersListState {
  list: DeveloperState[];
  activeDeveloperId: number;
}

export interface DeveloperState {
  id: number;
  firstName: string;
  lastName: string;
  role: string;
  frameworksList: string[];
}
