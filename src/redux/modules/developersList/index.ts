import { AnyAction } from "redux";
import { defaultDevsListState, DevelopersListState, DeveloperState } from "./model";

export const ADD_NEW_DEVELOPER = "ADD_NEW_DEVELOPER";
export const DELETE_DEVELOPER = "DELETE_DEVELOPER";
export const UPDATE_DEVELOPER = "UPDATE_DEVELOPER";
export const SET_ACTIVE_DEVELOPER = "SET_ACTIVE_DEVELOPER";

export default function reducer(
  state = defaultDevsListState,
  action: AnyAction,
): DevelopersListState {
  switch (action.type) {
    case ADD_NEW_DEVELOPER:
      const newDev = {
        id: Math.floor(Math.random() * 100),
        ...action.payload,
      };
      return {
        ...state,
        list: [
          ...state.list,
          newDev,
        ]
      };
    case DELETE_DEVELOPER:
      const filteredList = state.list
        ? state.list.filter((developer) => developer.id !== action.payload)
        : [];
      return {
        ...state,
        list: filteredList,
      };
    case UPDATE_DEVELOPER:
      const newDevId = action.payload.id;
      const reducedList = state.list
        ? state.list.filter((developer) => developer.id !== newDevId)
        : [];
      return {
        ...state,
        list: [
          ...reducedList,
          action.payload,
        ],
      };
    case SET_ACTIVE_DEVELOPER:
      return {
        ...state,
        activeDeveloperId: action.payload,
      };
    default:
      return state;
  }
}

export function addNewDeveloper(developer: DeveloperState): AnyAction {
  return {
    type: ADD_NEW_DEVELOPER,
    payload: developer,
  };
}

export function deleteDeveloper(id: number): AnyAction {
  return {
    type: DELETE_DEVELOPER,
    payload: id,
  };
}

export function updateDeveloper(developer: DeveloperState): AnyAction {
  return {
    type: UPDATE_DEVELOPER,
    payload: developer,
  };
}

export function setActiveDeveloperId(id: number): AnyAction {
  return {
    type: SET_ACTIVE_DEVELOPER,
    payload: id,
  };
}
