import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import developersList from "./developersList";
import { DevelopersListState } from "./developersList/model";
import enums from "./enums";
import { EnumsState } from "./enums/model";

export interface RootState {
  developersList: DevelopersListState;
  enums: EnumsState;
}

export default combineReducers({
  developersList,
  enums,
  form: formReducer,
});
