import { rolesList } from "../../../mock_data/roles_list";
import { frameworksList } from "../../../mock_data/frameworks_list";

export const defaultEnumsState: EnumsState = {
  roles: rolesList,
  frameworks: frameworksList,
};

export interface EnumsState {
  roles: string[];
  frameworks: string[];
}
