import { AnyAction } from "redux";
import { defaultEnumsState, EnumsState, } from "./model";

export default function reducer(
  state = defaultEnumsState,
  action: AnyAction,
): EnumsState {
  switch (action.type) {
    default:
      return state;
  }
}
