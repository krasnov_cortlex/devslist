import developersListSelectors from "./developersList";
import enumsSelectors from "./enums";

export default {
  ...developersListSelectors,
  ...enumsSelectors,
};
