import { RootState } from "../modules";

export function getAppDevelopersList(state: RootState): RootState["developersList"]["list"] {
  return (
    state &&
    state.developersList &&
    state.developersList.list
  ) || null;
}

export function getAppActiveDeveloperId(state: RootState): RootState["developersList"]["activeDeveloperId"] {
  return (
    state &&
    state.developersList &&
    state.developersList.activeDeveloperId
  ) || 0;
}

export default {
  getAppDevelopersList,
  getAppActiveDeveloperId,
};
