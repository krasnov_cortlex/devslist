import { RootState } from "../modules";

export function getAppRolesEnums(state: RootState): RootState["enums"]["roles"] {
  return (
    state &&
    state.enums &&
    state.enums.roles
  ) || null;
}

export function getAppFrameworksEnums(state: RootState): RootState["enums"]["frameworks"] {
  return (
    state &&
    state.enums &&
    state.enums.frameworks
  ) || null;
}

export default {
  getAppRolesEnums,
  getAppFrameworksEnums,
};
