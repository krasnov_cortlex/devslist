import React from "react";
import { Switch, Route } from "react-router-dom";
import DevelopersList from "./components/pages/DevelopersList";
import PageNotFound from "./components/pages/PageNotFound";
import Developer from "./components/pages/Developer";
import NewDeveloper from "./components/pages/NewDeveloper";

export default () => (
  <Switch>
    <Route exact path={"/"} component={DevelopersList} />
    <Route exact path={"/developer/add"} component={NewDeveloper} />
    <Route exact path={"/developer/edit/:developerId"} component={Developer} />
    <Route component={PageNotFound} />
  </Switch>
);
