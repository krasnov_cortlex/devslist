import lightGreen from "@material-ui/core/colors/lightGreen";
import red from "@material-ui/core/colors/red";
import grey from "@material-ui/core/colors/grey";

interface IndexedMapper {
  [key: string]: string;
}

const themeColorMapper: IndexedMapper = {
  success: lightGreen[500],
  warning: red[500],
  disabled: grey[400],
}

export default themeColorMapper;