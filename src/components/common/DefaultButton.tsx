import React from "react";
import styled from "styled-components";
import Button from '@material-ui/core/Button';
import themeColorMappers from "../../utils/themeColorMappers";

const StyledButton = styled(Button) <DefaultButtonOwnProps>`
  border-width: ${(props) => props.isTextButton ? 0 : 2}px;
  border-radius: 20px;
  border-color: ${(props) => themeColorMappers[props.theme]};
  color: ${(props) => themeColorMappers[props.theme]};
`;

interface DefaultButtonOwnProps {
  [key: string]: any;
  theme: string;
  isTextButton?: boolean;
}

function DefaultButton(props: DefaultButtonOwnProps) {
  return <StyledButton {...props} variant={"outlined"} disableElevation />;
}

export default DefaultButton
