import React, { Component } from "react";
import styled from "styled-components";
import { reduxForm, InjectedFormProps, Field } from "redux-form";
import { InputLabel, MenuItem } from "@material-ui/core";
import DefaultButton from "../DefaultButton";
import { renderCustomInput } from "./DeveloperFormFields/CustomInput";
import { renderCustomSelect } from "./DeveloperFormFields/CustomSelect";
import { renderCustomMultipleSelect } from "./DeveloperFormFields/CustomMultipleSelect";
import { DeveloperState } from "../../../redux/modules/developersList/model";

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 15px 0;
`;

interface DeveloperFormOwnProps {
  isEditForm?: boolean;
  developer?: DeveloperState;
  roles: string[] | null;
  frameworksList: string[] | null;
  onSubmitFire?: (id: number) => void;
}

const required = (value: string) => value ? undefined : "Required";

class DeveloperForm extends Component<DeveloperFormOwnProps & InjectedFormProps<DeveloperState, DeveloperFormOwnProps>> {
  public renderOptionsList = (list: string[]) => (
    list.map((item: string) => <MenuItem key={item} value={item}>{item}</MenuItem>)
  )

  render() {
    const {
      handleSubmit,
      isEditForm,
      onSubmitFire,
      developer,
      roles,
      frameworksList,
    } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <InputLabel>{"First Name"}</InputLabel>
        <Field
          name={"firstName"}
          component={renderCustomInput}
          validate={[required]}
          value={developer?.firstName}
        />

        <InputLabel>{"Last Name"}</InputLabel>
        <Field
          name={"lastName"}
          component={renderCustomInput}
          validate={[required]}
          value={developer?.lastName}
        />

        <InputLabel>{"Role"}</InputLabel>
        <Field name={"role"} component={renderCustomSelect} validate={[required]}>
          <MenuItem value={developer?.role}>
            <em>None</em>
          </MenuItem>
          {
            roles && roles.length
              ? this.renderOptionsList(roles)
              : null
          }
        </Field>

        <InputLabel>{"Frameworks"}</InputLabel>
        <Field name={"frameworksList"} component={renderCustomMultipleSelect} validate={[required]}>
          <MenuItem value={developer?.frameworksList}>
            <em>None</em>
          </MenuItem>
          {
            frameworksList && frameworksList.length
              ? this.renderOptionsList(frameworksList)
              : null
          }
        </Field>

        <ButtonsWrapper>
          <DefaultButton theme={"success"} type={"submit"}>
            {"Save"}
          </DefaultButton>
          {
            isEditForm && (
              <DefaultButton
                theme={"warning"}
                onClick={() => onSubmitFire && developer ? onSubmitFire(developer.id) : null}
              >
                {"Fire"}
              </DefaultButton>
            )
          }
        </ButtonsWrapper>
      </form>
    );
  }
}

export default reduxForm<DeveloperState, DeveloperFormOwnProps>({
  form: "developers",
})(DeveloperForm);
