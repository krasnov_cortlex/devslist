import React from "react";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { Chip } from "@material-ui/core";

export const renderCustomMultipleSelect = ({
  input,
  label,
  name,
  meta: { touched, error },
  childValuesMap = [],
  children,
  ...custom
}: any) => (
    <FormControl fullWidth margin={"dense"} error={touched && !!error}>
      <Select
        {...input}
        {...custom}
        value={input.value || []}
        variant={"outlined"}
        displayEmpty
        multiple
        renderValue={(selected: any) => (
          <div>
            {selected.map((value: string) => (
              <Chip
                key={value}
                size={"small"}
                label={(childValuesMap.find((item: any) => item.value === value) || {}).label || value}
              />
            ))}
          </div>
        )}
      >
        {children}
      </Select>
    </FormControl>
  )
