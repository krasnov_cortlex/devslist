import React from "react";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

export const renderCustomSelect = ({
  input,
  label,
  name,
  meta: { touched, error },
  children,
  ...custom
}: any) => (
    <FormControl fullWidth margin={"dense"} error={touched && !!error}>
      <Select
        {...input}
        {...custom}
        variant={"outlined"}
        value={input.value || ""}
        displayEmpty
      >
        {children}
      </Select>
    </FormControl>
  )
