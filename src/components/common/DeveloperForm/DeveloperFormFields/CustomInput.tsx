import React from "react";
import { TextField, FormControl } from "@material-ui/core";

export const renderCustomInput = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}: any) => (
    <FormControl fullWidth margin={"dense"}>
      <TextField
        label={label}
        variant={"outlined"}
        size={"small"}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
      />
    </FormControl>
  )
