import React from "react";

interface HeaderTitleIconOwnProps {
  icon?: string;
}

function HeaderTitleIcon(props: HeaderTitleIconOwnProps) {
  switch (props.icon) {
    case "devsList":
      return <span role={"img"} aria-label={"developer with laptop"}>👨‍💻</span>
    case "newDeveloper":
      return <span role={"img"} aria-label={"hands shaking"}>🤝</span>
    default:
      return <span role={"img"} aria-label={"unknown owl"}>🦉</span>
  }
}

export default HeaderTitleIcon;
