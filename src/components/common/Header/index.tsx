import React, { ReactNode } from "react";
import styled from "styled-components";
import Container from '@material-ui/core/Container';
import HeaderTitle from "./HeaderTitle";

const FlexContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Wrapper = styled.div`
  background: #fff;
`;

interface HeaderOwnProps {
  icon?: string;
  children?: ReactNode;
  title: string;
}

function Header(props: HeaderOwnProps) {
  return (
    <Wrapper>
      <Container maxWidth={"lg"}>
        <FlexContainer>
          <HeaderTitle text={props.title} icon={props.icon} />
          <div>
            {props.children}
          </div>
        </FlexContainer>
      </Container>
    </Wrapper>
  );
}

export default Header;
