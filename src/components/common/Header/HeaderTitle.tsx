import React from "react";
import styled from "styled-components";
import HeaderTitleIcon from './HeaderTitleIcon';

const FlexContainer = styled.div`
  display: flex;
  align-items: center;
  font-size: 24px;
  font-weight: 700;

  > span {
    margin-right: 10px;
    line-height: 3;
  }
`;

const HeaderTitleText = styled.span`
  text-transform: capitalize;
`;

interface HeaderTitleOwnProps {
  text: string;
  icon?: string;
}

function HeaderTitle(props: HeaderTitleOwnProps) {
  return (
    <FlexContainer>
      <HeaderTitleIcon icon={props.icon} />
      <HeaderTitleText>{props.text}</HeaderTitleText>
    </FlexContainer>
  );
}

export default HeaderTitle;
