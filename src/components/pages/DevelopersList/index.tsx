import React from "react";
import { connect } from "react-redux";
import history from "../../../history";
import { RootState } from "../../../redux/modules";
import selectors from "../../../redux/selectors";
import {
  Container,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import Header from "../../common/Header";
import DevsListRow from "./DevsListRow";
import DefaultButton from "../../common/DefaultButton";
import { deleteDeveloper } from "../../../redux/modules/developersList/index";
import { DeveloperState } from "../../../redux/modules/developersList/model";

interface DevelopersListProps {
  developersList: RootState["developersList"]["list"];
  deleteDeveloper: (id: number) => void;
}

function DevelopersList(props: DevelopersListProps) {
  const { developersList, deleteDeveloper } = props;
  const handleDeleteDeveloper = (id: number) => deleteDeveloper(id);

  const renderRow = (developer: DeveloperState, index: number) => {
    return (
      <DevsListRow
        key={`${developer.id}/${index}`}
        handleDeleteDeveloper={handleDeleteDeveloper}
        {...developer}
      />
    )
  }

  return (
    <>
      <Header icon={"devsList"} title={"developers"}>
        <DefaultButton
          theme={"success"}
          onClick={() => history.push("/developer/add")}
        >
          {"Hire"}
        </DefaultButton>
      </Header>
      <Container maxWidth={"lg"}>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>{"First Name"}</TableCell>
                <TableCell>{"Last Name"}</TableCell>
                <TableCell>{"Role"}</TableCell>
                <TableCell>{"Frameworks"}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {
                developersList.length
                  ? developersList.map(renderRow)
                  : null
              }
            </TableBody>
            {
              !developersList.length
                ? <caption>{"Sorry, nothing was found!"}</caption>
                : null
            }
          </Table>
        </TableContainer>
      </Container>
    </>
  );
}

const mapStateToProps = (state: RootState) => ({
  developersList: selectors.getAppDevelopersList(state),
});

const mapDispatchToProps = ({
  deleteDeveloper,
});

export default connect(mapStateToProps, mapDispatchToProps)(DevelopersList);
