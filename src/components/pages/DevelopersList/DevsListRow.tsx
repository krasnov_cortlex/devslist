import React from "react";
import styled from "styled-components";
import { TableRow, TableCell, Chip } from "@material-ui/core";
import DefaultButton from "../../common/DefaultButton";
import history from "../../../history";
import { setActiveDeveloperId } from "../../../redux/modules/developersList/index";
import { connect } from "react-redux";

const StyledTableRow = styled(TableRow)`
  background: white;
  border-radius: 5px;
`;

const StyledChip = styled(Chip)`
  margin-right: 5px;
`;

const ButtonsWrapper = styled.div`
  > button {
    margin-left: 10px;
  }
`;

interface TableRowOwnProps {
  id: number;
  firstName: string;
  lastName: string;
  role: string;
  frameworksList: string[];
  handleDeleteDeveloper: (id: number) => void;
  setActiveDeveloperId: (id: number) => void;
}

function DevsListRow(props: TableRowOwnProps) {
  const {
    id,
    firstName,
    lastName,
    role,
    frameworksList,
    handleDeleteDeveloper,
  } = props;

  const renderFrameworkChips = (frameworksList: string[]) => {
    return frameworksList && frameworksList.length
      ? frameworksList.map((item: string) => <StyledChip key={item} size={"small"} label={item} />)
      : null;
  };


  const handleEditButtonClick = (id: number) => {
    history.push(`/developer/edit/${id}`);
    props.setActiveDeveloperId(id);
  };

  const getAvatarByDeveloper = (id: number) => <div>image</div>;

  return (
    <StyledTableRow>
      <TableCell>{getAvatarByDeveloper(id)}</TableCell>
      <TableCell>{firstName}</TableCell>
      <TableCell>{lastName}</TableCell>
      <TableCell>{role}</TableCell>
      <TableCell>{renderFrameworkChips(frameworksList)}</TableCell>
      <TableCell align={"right"}>
        <ButtonsWrapper>
          <DefaultButton
            theme={"disabled"}
            onClick={() => handleEditButtonClick(id)}
            isTextButton
          >
            {"Edit"}
          </DefaultButton>
          <DefaultButton
            theme={"warning"}
            onClick={() => handleDeleteDeveloper(id)}
          >
            {"Fire"}
          </DefaultButton>
        </ButtonsWrapper>
      </TableCell>
    </StyledTableRow>
  )
}

const mapDispatchToProps = ({
  setActiveDeveloperId,
})

export default connect(null, mapDispatchToProps)(DevsListRow);
