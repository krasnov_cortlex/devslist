import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import selectors from "../../redux/selectors";
import { RootState } from "../../redux/modules/index";
import { Link } from "react-router-dom";
import { DeveloperState } from "../../redux/modules/developersList/model";
import { updateDeveloper, deleteDeveloper } from "../../redux/modules/developersList";
import Header from "../common/Header";
import { Container } from '@material-ui/core';
import DeveloperForm from "../common/DeveloperForm/DeveloperForm";
import { setActiveDeveloperId } from "../../redux/modules/developersList/index";

const StyledNavPanel = styled.nav`
  padding: 30px 0;
`;

interface DeveloperProps {
  updateDeveloper: (user: DeveloperState) => void;
  deleteDeveloper: (id: number) => void;
  roles: string[] | null;
  frameworksList: string[] | null;
  developersList: DeveloperState[] | null;
  activeDeveloperId: number;
}

function Developer(props: DeveloperProps) {
  const handleSubmitSave = (user: DeveloperState) => {
    props.updateDeveloper(user);
  }

  const handleSubmitFire = (id: number) => {
    props.deleteDeveloper(id);
  }

  const getActiveDeveloperData = (id: number) => {
    return props.developersList
      ? props.developersList.find((developer: DeveloperState) => developer.id = id)
      : undefined
  }

  return (
    <>
      <Header title={"developers"} />
      <Container maxWidth={"lg"}>
        <StyledNavPanel>
          <Link to={"/"}>{"Back to developers"}</Link>
        </StyledNavPanel>
        <DeveloperForm
          isEditForm
          onSubmit={handleSubmitSave}
          onSubmitFire={handleSubmitFire}
          roles={props.roles}
          frameworksList={props.frameworksList}
          developer={getActiveDeveloperData(props.activeDeveloperId)}
        />
      </Container>
    </>
  );
}

const mapStateToProps = (state: RootState) => ({
  developersList: selectors.getAppDevelopersList(state),
  activeDeveloperId: selectors.getAppActiveDeveloperId(state),
  roles: selectors.getAppRolesEnums(state),
  frameworksList: selectors.getAppFrameworksEnums(state),
})

const mapDispatchToProps = ({
  updateDeveloper,
  deleteDeveloper,
  setActiveDeveloperId,
})

export default connect(mapStateToProps, mapDispatchToProps)(Developer);
