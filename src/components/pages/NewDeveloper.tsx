import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import selectors from "../../redux/selectors";
import { RootState } from "../../redux/modules";
import { Container } from "@material-ui/core";
import Header from "../common/Header";
import DeveloperForm from "../common/DeveloperForm/DeveloperForm";
import { DeveloperState } from "../../redux/modules/developersList/model";
import { addNewDeveloper } from "../../redux/modules/developersList";

const StyledNavPanel = styled.nav`
  padding: 30px 0;
`;

interface NewDeveloperProps {
  addNewDeveloper: (developer: DeveloperState) => void;
  roles: string[] | null;
  frameworksList: string[] | null;
}

function NewDeveloper(props: NewDeveloperProps) {
  const handleSubmitSave = (user: DeveloperState) => {
    props.addNewDeveloper(user);
  }

  return (
    <>
      <Header icon={"newDeveloper"} title={"Hire"} />
      <Container maxWidth={"lg"}>
        <StyledNavPanel>
          <Link to={"/"}>{"Back to developers"}</Link>
        </StyledNavPanel>
        <DeveloperForm
          onSubmit={handleSubmitSave}
          roles={props.roles}
          frameworksList={props.frameworksList}
        />
      </Container>
    </>
  );
}

const mapStateToProps = (state: RootState) => ({
  roles: selectors.getAppRolesEnums(state),
  frameworksList: selectors.getAppFrameworksEnums(state),
})

const mapDispatchToProps = ({
  addNewDeveloper,
})

export default connect(mapStateToProps, mapDispatchToProps)(NewDeveloper);
