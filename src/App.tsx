import React from "react";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/configureStore";
import history from "./history";
import Routes from "./Routes";
import CssBaseline from "@material-ui/core/CssBaseline";
import { GlobalStyling } from './utils/globalStyling';

function App() {
  return (
    <Provider store={store}>
      <CssBaseline />
      <GlobalStyling />
      <Router history={history}>
        <Routes />
      </Router>
    </Provider>
  );
}

export default App;
