interface Developer {
  id: number;
  firstName: string;
  lastName: string;
  role: string;
  frameworksList: string[];
}

const devsList: Developer[] = [
  {
    id: 1,
    firstName: "Nick",
    lastName: "Smet",
    role: "full-stack",
    frameworksList: [
      "react",
      "laravel",
    ],
  },
  {
    id: 2,
    firstName: "Wim",
    lastName: "Van Aerschot",
    role: "full-stack",
    frameworksList: [
      "react",
      "vue",
    ],
  },
  {
    id: 3,
    firstName: "Fredrick",
    lastName: "Humblet",
    role: "full-stack",
    frameworksList: [
      "react",
    ],
  },
  {
    id: 4,
    firstName: "Elon",
    lastName: "Mulder",
    role: "back-end",
    frameworksList: [
      "ember",
      "angular",
    ],
  },
  {
    id: 5,
    firstName: "Karel",
    lastName: "Eckelaert",
    role: "back-end",
    frameworksList: [
      "laravel",
    ],
  },
  {
    id: 6,
    firstName: "Andries",
    lastName: "Demeuleanaere",
    role: "front-end",
    frameworksList: [
      "react",
    ],
  },
  {
    id: 7,
    firstName: "Jessy",
    lastName: "Cordemans",
    role: "front-end",
    frameworksList: [
      "react",
    ],
  },
];

export default devsList;
