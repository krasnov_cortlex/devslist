interface Developer {
  firstName: string;
  lastName: string;
  role: string;
  frameworksList: string[];
}

const devsList: Developer[] = [
  {
    firstName: "Nick",
    lastName: "Smet",
    role: "full-stack",
    frameworksList: [
      "react",
      "laravel",
    ],
  },
  {
    firstName: "Wim",
    lastName: "Van Aerschot",
    role: "full-stack",
    frameworksList: [
      "react",
      "vue",
    ],
  },
  {
    firstName: "Fredrick",
    lastName: "Humblet",
    role: "full-stack",
    frameworksList: [
      "react",
    ],
  },
  {
    firstName: "Elon",
    lastName: "Mulder",
    role: "back-end",
    frameworksList: [
      "ember",
      "angular",
    ],
  },
  {
    firstName: "Karel",
    lastName: "Eckelaert",
    role: "back-end",
    frameworksList: [
      "laravel",
    ],
  },
  {
    firstName: "Andries",
    lastName: "Demeuleanaere",
    role: "front-end",
    frameworksList: [
      "react",
    ],
  },
  {
    firstName: "Jessy",
    lastName: "Cordemans",
    role: "front-end",
    frameworksList: [
      "react",
    ],
  },
];

export default devsList;
